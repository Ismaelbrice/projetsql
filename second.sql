-- Active: 1709906099311@@127.0.0.1@3306@projetsql

SELECT * FROM promo;


SELECT student.name ,promo.name 
FROM student INNER JOIN promo_student ON student.id = id_student 
INNER JOIN promo ON id_promo=promo.id;

SELECT room.name,center.name FROM room INNER JOIN center ON id_center=center.id;

SELECT center.name,room.name FROM center LEFT JOIN room ON id_center=center.id ORDER BY center.name;

UPDATE room SET capacity=10 WHERE id=1;

SELECT * FROM room;

SELECT region.name 'Région',center.name 'Centre' FROM center
INNER JOIN region ON id_region=region.id ORDER BY region.name ;

SELECT center.name 'Centre',promo.name 'Promo' FROM center
INNER JOIN promo ON id_center=center.id ORDER BY center.name;


--supprimer les student de la table
TRUNCATE TABLE student;

--mise a jour des student
UPDATE student SET name='amine' WHERE id=3;

--insérer student
INSERT INTO student (name) VALUES
('Chieko');

DELETE FROM student
WHERE id=6;

SELECT room.name ,booking.startDate ,booking.endDate FROM room
LEFT JOIN booking ON room.id=id_room;

SELECT promo.name,booking.startDate ,booking.endDate from promo
LEFT JOIN booking on promo.id=id_promo;

UPDATE booking SET startDate='2024-03-01',endDate='2025-02-01' WHERE id_room=1;

UPDATE booking SET startDate='2024-01-02',endDate='2024-01-03' WHERE promo=2;

DELETE FROM booking
WHERE id=6;
