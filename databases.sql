-- Active: 1709906099311@@127.0.0.1@3306@projetsql

DROP TABLE IF EXISTS promo_student;
DROP TABLE IF EXISTS student;
DROP TABLE IF EXISTS booking;
DROP TABLE IF EXISTS promo;
DROP TABLE IF EXISTS room;
DROP TABLE IF EXISTS center;
DROP TABLE IF EXISTS region;



CREATE TABLE region (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64)
);

CREATE TABLE center (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    id_region INT,
    Foreign Key (id_region) REFERENCES region(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE room (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    capacity INT,
    color VARCHAR(20),
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE promo (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64),
    startDate DATE,
    endDate DATE,
    studentNumber INT,
    id_center INT,
    Foreign Key (id_center) REFERENCES center(id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE booking (
    id INT PRIMARY KEY AUTO_INCREMENT,
    startDate DATE,
    endDate DATE,
    id_room INT,
    id_promo INT,
    Foreign Key (id_room) REFERENCES room(id) ON DELETE CASCADE ON UPDATE CASCADE,
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE CASCADE ON UPDATE CASCADE
    
);

CREATE TABLE student (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64)
);

CREATE TABLE promo_student (
    id_promo INT,
    id_student INT,
    PRIMARY KEY (id_promo,id_student),
    Foreign Key (id_promo) REFERENCES promo(id) ON DELETE CASCADE ON UPDATE CASCADE,
    Foreign Key (id_student) REFERENCES student(id) ON DELETE CASCADE ON UPDATE CASCADE
);

INSERT INTO region (name) VALUES
('Rhone'),
('Auvergne'),
('Alsace'),
('Bourgogne'),
('PACA');

INSERT INTO center (name, id_region) VALUES
('simplon', 1),
('arat', 2),
('poten', 3),
('tardi', 4),
('hattep', 5);

INSERT INTO room (name, capacity, color, id_center) VALUES
('classehtml', 20, 'vert', 1),
('classecss', 25, 'rouge', 2),
('classejava', 8, 'violet', 3),
('classjs', 30, 'bleu', 4),
('classphp', 30, 'jaune', 5);

INSERT INTO promo (name, startDate, endDate, studentNumber, id_center) VALUES
('promo1', '2024-01-01', '2024-12-31', 20, 1),
('promo2', '2024-01-02', '2024-12-31', 19, 2),
('promo3', '2024-01-03', '2024-12-31', 8, 3),
('promo4', '2023-01-04', '2023-12-31', 32, 4),
('promo5', '2024-01-05', '2024-12-31', 21, 5);

INSERT INTO student (name) VALUES
('Isma'),
('elamine'),
('romane'),
('juan'),
('carlos');

INSERT INTO booking (startDate, endDate, id_room, id_promo) VALUES
('2023-01-02', '2023-04-03', 1, 1),
('2023-02-02', '2023-05-03', 2, 2),
('2023-03-02', '2023-06-03', 3, 3),
('2023-04-02', '2023-07-03', 4, 4),
('2023-05-02', '2023-08-03', 5, 5);

INSERT INTO promo_student (id_promo, id_student) VALUES
(1, 1),
(1, 2),
(2, 3),
(2, 4),
(3, 1);